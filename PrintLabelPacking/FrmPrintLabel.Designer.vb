﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmPrintLabel
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.BCTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel24 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.CasenoLabel = New MetroFramework.Controls.MetroLabel()
        Me.PackgradeLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.PrintTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.PackedGradeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.SpShippingSELPackedgradeByInternalGradeBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcessingDataSet1 = New PrintLabelPacking.ProcessingDataSet()
        Me.SpShippingSELPackedgradeByInternalGradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SpPDSELREPRINTLABELBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SpShippingSELPackedgradeByGradersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Packing_GET_DataByBCBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Packing_GET_DataByBCTableAdapter = New PrintLabelPacking.ProcessingDataSetTableAdapters.sp_Packing_GET_DataByBCTableAdapter()
        Me.CropComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.SpShippingSELCropFromPackedgradeBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.SpShippingSELCropFromPackedgradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.TypeBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.TypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.GradeTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.CaseNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.ChkRENew = New System.Windows.Forms.CheckBox()
        Me.Rdt384M = New System.Windows.Forms.RadioButton()
        Me.RdtZebra = New System.Windows.Forms.RadioButton()
        Me.Rdt246MP = New System.Windows.Forms.RadioButton()
        Me.NetDefTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.BoxTareTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.GrossDefTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel12 = New MetroFramework.Controls.MetroLabel()
        Me.BarcodeTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel13 = New MetroFramework.Controls.MetroLabel()
        Me.NetRealTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel14 = New MetroFramework.Controls.MetroLabel()
        Me.Sp_PD_SEL_REPRINTLABELBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_PD_SEL_REPRINTLABELTableAdapter1 = New PrintLabelPacking.ProcessingDataSetTableAdapters.sp_PD_SEL_REPRINTLABELTableAdapter()
        Me.MetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.GradersDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GradeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BarcodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CasenoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetRealDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductionTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BoxtareDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GrossdefDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GrossrealDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PdremarkDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToCustomer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.packingdate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.packingtime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpPDSELREPRINTLABELBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.TypeTableAdapter1 = New PrintLabelPacking.ProcessingDataSetTableAdapters.typeTableAdapter()
        Me.Sp_Shipping_SEL_CropFromPackedgradeTableAdapter1 = New PrintLabelPacking.ProcessingDataSetTableAdapters.sp_Shipping_SEL_CropFromPackedgradeTableAdapter()
        Me.Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter1 = New PrintLabelPacking.ProcessingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter()
        Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter1 = New PrintLabelPacking.ProcessingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByGradersTableAdapter()
        Me.pd_ryo_customer_label_setupBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.pd_ryo_customer_label_setupTableAdapter = New PrintLabelPacking.ProcessingDataSetTableAdapters.pd_ryo_customer_label_setupTableAdapter()
        Me.TableAdapterManager1 = New PrintLabelPacking.ProcessingDataSetTableAdapters.TableAdapterManager()
        Me.Graders = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Grade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Caseno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Net = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetReal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductionType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.boxtare = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grossdef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grossreal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pdremark = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fromcuslotno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUSMOISTUREDEFAULT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.accurunno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.caserunno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cusrunno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUSGRADECODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUSNETDEFAULT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUSGROSSDEFAULT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUSPRINTFORMATNAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STATUSS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.customer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BCCustomer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.taredef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CountOfLabel = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel15 = New MetroFramework.Controls.MetroLabel()
        Me.Rdt247 = New System.Windows.Forms.RadioButton()
        Me.Rdt384MT = New System.Windows.Forms.RadioButton()
        CType(Me.SpShippingSELPackedgradeByInternalGradeBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcessingDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELPackedgradeByInternalGradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpPDSELREPRINTLABELBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELPackedgradeByGradersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Packing_GET_DataByBCBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELCropFromPackedgradeBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELCropFromPackedgradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TypeBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_PD_SEL_REPRINTLABELBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpPDSELREPRINTLABELBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pd_ryo_customer_label_setupBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BCTextbox
        '
        Me.BCTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.BCTextbox.CustomButton.Image = Nothing
        Me.BCTextbox.CustomButton.Location = New System.Drawing.Point(53, 2)
        Me.BCTextbox.CustomButton.Name = ""
        Me.BCTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.BCTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.BCTextbox.CustomButton.TabIndex = 1
        Me.BCTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.BCTextbox.CustomButton.UseSelectable = True
        Me.BCTextbox.CustomButton.Visible = False
        Me.BCTextbox.Enabled = False
        Me.BCTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.BCTextbox.Lines = New String(-1) {}
        Me.BCTextbox.Location = New System.Drawing.Point(838, -5)
        Me.BCTextbox.MaxLength = 32767
        Me.BCTextbox.Name = "BCTextbox"
        Me.BCTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.BCTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.BCTextbox.SelectedText = ""
        Me.BCTextbox.SelectionLength = 0
        Me.BCTextbox.SelectionStart = 0
        Me.BCTextbox.ShortcutsEnabled = True
        Me.BCTextbox.Size = New System.Drawing.Size(81, 30)
        Me.BCTextbox.Style = MetroFramework.MetroColorStyle.Magenta
        Me.BCTextbox.TabIndex = 135
        Me.BCTextbox.UseSelectable = True
        Me.BCTextbox.UseStyleColors = True
        Me.BCTextbox.Visible = False
        Me.BCTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.BCTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Enabled = False
        Me.MetroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel1.Location = New System.Drawing.Point(755, -5)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(74, 25)
        Me.MetroLabel1.TabIndex = 134
        Me.MetroLabel1.Text = "Barcode"
        Me.MetroLabel1.Visible = False
        '
        'MetroLabel24
        '
        Me.MetroLabel24.AutoSize = True
        Me.MetroLabel24.Location = New System.Drawing.Point(1221, 99)
        Me.MetroLabel24.Name = "MetroLabel24"
        Me.MetroLabel24.Size = New System.Drawing.Size(36, 19)
        Me.MetroLabel24.TabIndex = 137
        Me.MetroLabel24.Text = "print"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Enabled = False
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.Location = New System.Drawing.Point(925, -9)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(77, 25)
        Me.MetroLabel2.TabIndex = 138
        Me.MetroLabel2.Text = "Case no."
        Me.MetroLabel2.Visible = False
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Enabled = False
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel3.Location = New System.Drawing.Point(1070, -5)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(90, 25)
        Me.MetroLabel3.TabIndex = 140
        Me.MetroLabel3.Text = "Packgrade"
        Me.MetroLabel3.Visible = False
        '
        'CasenoLabel
        '
        Me.CasenoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CasenoLabel.Enabled = False
        Me.CasenoLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.CasenoLabel.Location = New System.Drawing.Point(1008, -9)
        Me.CasenoLabel.Name = "CasenoLabel"
        Me.CasenoLabel.Size = New System.Drawing.Size(67, 34)
        Me.CasenoLabel.TabIndex = 141
        Me.CasenoLabel.Text = "MetroLabel4"
        Me.CasenoLabel.Visible = False
        '
        'PackgradeLabel
        '
        Me.PackgradeLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PackgradeLabel.Enabled = False
        Me.PackgradeLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.PackgradeLabel.Location = New System.Drawing.Point(1169, -14)
        Me.PackgradeLabel.Name = "PackgradeLabel"
        Me.PackgradeLabel.Size = New System.Drawing.Size(83, 34)
        Me.PackgradeLabel.TabIndex = 142
        Me.PackgradeLabel.Text = "MetroLabel5"
        Me.PackgradeLabel.Visible = False
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Enabled = False
        Me.MetroLabel4.Location = New System.Drawing.Point(719, 5)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(40, 19)
        Me.MetroLabel4.TabIndex = 144
        Me.MetroLabel4.Text = "Clear"
        Me.MetroLabel4.Visible = False
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Enabled = False
        Me.MetroTile1.Location = New System.Drawing.Point(663, -23)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(50, 48)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 143
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.PrintLabelPacking.My.Resources.Resources.StopProperty64
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        Me.MetroTile1.Visible = False
        '
        'PrintTile
        '
        Me.PrintTile.ActiveControl = Nothing
        Me.PrintTile.AutoSize = True
        Me.PrintTile.BackColor = System.Drawing.Color.White
        Me.PrintTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PrintTile.Location = New System.Drawing.Point(1221, 44)
        Me.PrintTile.Name = "PrintTile"
        Me.PrintTile.Size = New System.Drawing.Size(50, 48)
        Me.PrintTile.Style = MetroFramework.MetroColorStyle.White
        Me.PrintTile.TabIndex = 136
        Me.PrintTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.PrintTile.TileImage = Global.PrintLabelPacking.My.Resources.Resources.Paper64Copy
        Me.PrintTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.PrintTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.PrintTile.UseSelectable = True
        Me.PrintTile.UseTileImage = True
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(164, 71)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(86, 19)
        Me.MetroLabel6.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel6.TabIndex = 200
        Me.MetroLabel6.Text = "PackedGrade"
        '
        'PackedGradeComboBox
        '
        Me.PackedGradeComboBox.DataSource = Me.SpShippingSELPackedgradeByInternalGradeBindingSource1
        Me.PackedGradeComboBox.DisplayMember = "packedgrade"
        Me.PackedGradeComboBox.FormattingEnabled = True
        Me.PackedGradeComboBox.ItemHeight = 23
        Me.PackedGradeComboBox.Location = New System.Drawing.Point(261, 68)
        Me.PackedGradeComboBox.Name = "PackedGradeComboBox"
        Me.PackedGradeComboBox.Size = New System.Drawing.Size(226, 29)
        Me.PackedGradeComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.PackedGradeComboBox.TabIndex = 197
        Me.PackedGradeComboBox.UseSelectable = True
        Me.PackedGradeComboBox.ValueMember = "packedgrade"
        '
        'SpShippingSELPackedgradeByInternalGradeBindingSource1
        '
        Me.SpShippingSELPackedgradeByInternalGradeBindingSource1.DataMember = "sp_Shipping_SEL_PackedgradeByInternalGrade"
        Me.SpShippingSELPackedgradeByInternalGradeBindingSource1.DataSource = Me.ProcessingDataSet1
        '
        'ProcessingDataSet1
        '
        Me.ProcessingDataSet1.DataSetName = "ProcessingDataSet"
        Me.ProcessingDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SpShippingSELPackedgradeByInternalGradeBindingSource
        '
        Me.SpShippingSELPackedgradeByInternalGradeBindingSource.DataMember = "sp_Shipping_SEL_PackedgradeByInternalGrade"
        '
        'SpPDSELREPRINTLABELBindingSource
        '
        Me.SpPDSELREPRINTLABELBindingSource.DataMember = "sp_PD_SEL_REPRINTLABEL"
        '
        'SpShippingSELPackedgradeByGradersBindingSource
        '
        Me.SpShippingSELPackedgradeByGradersBindingSource.DataMember = "sp_Shipping_SEL_PackedgradeByGraders"
        '
        'Sp_Packing_GET_DataByBCBindingSource
        '
        Me.Sp_Packing_GET_DataByBCBindingSource.DataMember = "sp_Packing_GET_DataByBC"
        Me.Sp_Packing_GET_DataByBCBindingSource.DataSource = Me.ProcessingDataSet1
        '
        'Sp_Packing_GET_DataByBCTableAdapter
        '
        Me.Sp_Packing_GET_DataByBCTableAdapter.ClearBeforeFill = True
        '
        'CropComboBox
        '
        Me.CropComboBox.DataSource = Me.SpShippingSELCropFromPackedgradeBindingSource1
        Me.CropComboBox.DisplayMember = "crop"
        Me.CropComboBox.FormattingEnabled = True
        Me.CropComboBox.ItemHeight = 23
        Me.CropComboBox.Location = New System.Drawing.Point(68, 63)
        Me.CropComboBox.Name = "CropComboBox"
        Me.CropComboBox.Size = New System.Drawing.Size(90, 29)
        Me.CropComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CropComboBox.TabIndex = 225
        Me.CropComboBox.UseSelectable = True
        Me.CropComboBox.ValueMember = "crop"
        '
        'SpShippingSELCropFromPackedgradeBindingSource1
        '
        Me.SpShippingSELCropFromPackedgradeBindingSource1.DataMember = "sp_Shipping_SEL_CropFromPackedgrade"
        Me.SpShippingSELCropFromPackedgradeBindingSource1.DataSource = Me.ProcessingDataSet1
        '
        'SpShippingSELCropFromPackedgradeBindingSource
        '
        Me.SpShippingSELCropFromPackedgradeBindingSource.DataMember = "sp_Shipping_SEL_CropFromPackedgrade"
        '
        'TypeComboBox
        '
        Me.TypeComboBox.DataSource = Me.TypeBindingSource1
        Me.TypeComboBox.DisplayMember = "type"
        Me.TypeComboBox.FormattingEnabled = True
        Me.TypeComboBox.ItemHeight = 23
        Me.TypeComboBox.Location = New System.Drawing.Point(68, 99)
        Me.TypeComboBox.Name = "TypeComboBox"
        Me.TypeComboBox.Size = New System.Drawing.Size(90, 29)
        Me.TypeComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TypeComboBox.TabIndex = 226
        Me.TypeComboBox.UseSelectable = True
        Me.TypeComboBox.ValueMember = "type"
        '
        'TypeBindingSource1
        '
        Me.TypeBindingSource1.DataMember = "type"
        Me.TypeBindingSource1.DataSource = Me.ProcessingDataSet1
        '
        'TypeBindingSource
        '
        Me.TypeBindingSource.DataMember = "type"
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(23, 108)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(36, 19)
        Me.MetroLabel5.TabIndex = 228
        Me.MetroLabel5.Text = "Type"
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(23, 71)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(39, 19)
        Me.MetroLabel7.TabIndex = 227
        Me.MetroLabel7.Text = "Crop"
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(940, 23)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(62, 19)
        Me.MetroLabel8.TabIndex = 230
        Me.MetroLabel8.Text = "Case No."
        Me.MetroLabel8.Visible = False
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.Location = New System.Drawing.Point(785, 24)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(45, 19)
        Me.MetroLabel9.TabIndex = 229
        Me.MetroLabel9.Text = "Grade"
        Me.MetroLabel9.Visible = False
        '
        'GradeTextBox
        '
        Me.GradeTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.GradeTextBox.CustomButton.Image = Nothing
        Me.GradeTextBox.CustomButton.Location = New System.Drawing.Point(122, 2)
        Me.GradeTextBox.CustomButton.Name = ""
        Me.GradeTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.GradeTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.GradeTextBox.CustomButton.TabIndex = 1
        Me.GradeTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.GradeTextBox.CustomButton.UseSelectable = True
        Me.GradeTextBox.CustomButton.Visible = False
        Me.GradeTextBox.Enabled = False
        Me.GradeTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.GradeTextBox.Lines = New String(-1) {}
        Me.GradeTextBox.Location = New System.Drawing.Point(784, 45)
        Me.GradeTextBox.MaxLength = 32767
        Me.GradeTextBox.Name = "GradeTextBox"
        Me.GradeTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.GradeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.GradeTextBox.SelectedText = ""
        Me.GradeTextBox.SelectionLength = 0
        Me.GradeTextBox.SelectionStart = 0
        Me.GradeTextBox.ShortcutsEnabled = True
        Me.GradeTextBox.Size = New System.Drawing.Size(150, 30)
        Me.GradeTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.GradeTextBox.TabIndex = 231
        Me.GradeTextBox.UseSelectable = True
        Me.GradeTextBox.UseStyleColors = True
        Me.GradeTextBox.Visible = False
        Me.GradeTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.GradeTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'CaseNoTextBox
        '
        Me.CaseNoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.CaseNoTextBox.CustomButton.Image = Nothing
        Me.CaseNoTextBox.CustomButton.Location = New System.Drawing.Point(55, 2)
        Me.CaseNoTextBox.CustomButton.Name = ""
        Me.CaseNoTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.CaseNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CaseNoTextBox.CustomButton.TabIndex = 1
        Me.CaseNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CaseNoTextBox.CustomButton.UseSelectable = True
        Me.CaseNoTextBox.CustomButton.Visible = False
        Me.CaseNoTextBox.Enabled = False
        Me.CaseNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CaseNoTextBox.Lines = New String(-1) {}
        Me.CaseNoTextBox.Location = New System.Drawing.Point(939, 44)
        Me.CaseNoTextBox.MaxLength = 32767
        Me.CaseNoTextBox.Name = "CaseNoTextBox"
        Me.CaseNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CaseNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CaseNoTextBox.SelectedText = ""
        Me.CaseNoTextBox.SelectionLength = 0
        Me.CaseNoTextBox.SelectionStart = 0
        Me.CaseNoTextBox.ShortcutsEnabled = True
        Me.CaseNoTextBox.Size = New System.Drawing.Size(83, 30)
        Me.CaseNoTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CaseNoTextBox.TabIndex = 232
        Me.CaseNoTextBox.UseSelectable = True
        Me.CaseNoTextBox.UseStyleColors = True
        Me.CaseNoTextBox.Visible = False
        Me.CaseNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CaseNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'ChkRENew
        '
        Me.ChkRENew.AutoSize = True
        Me.ChkRENew.Location = New System.Drawing.Point(695, 72)
        Me.ChkRENew.Name = "ChkRENew"
        Me.ChkRENew.Size = New System.Drawing.Size(63, 17)
        Me.ChkRENew.TabIndex = 233
        Me.ChkRENew.Text = "Change"
        Me.ChkRENew.UseVisualStyleBackColor = True
        '
        'Rdt384M
        '
        Me.Rdt384M.AutoSize = True
        Me.Rdt384M.Location = New System.Drawing.Point(302, 109)
        Me.Rdt384M.Name = "Rdt384M"
        Me.Rdt384M.Size = New System.Drawing.Size(79, 17)
        Me.Rdt384M.TabIndex = 234
        Me.Rdt384M.Text = "TSC 384 M"
        Me.Rdt384M.UseVisualStyleBackColor = True
        '
        'RdtZebra
        '
        Me.RdtZebra.AutoSize = True
        Me.RdtZebra.Location = New System.Drawing.Point(413, 109)
        Me.RdtZebra.Name = "RdtZebra"
        Me.RdtZebra.Size = New System.Drawing.Size(53, 17)
        Me.RdtZebra.TabIndex = 235
        Me.RdtZebra.TabStop = True
        Me.RdtZebra.Text = "Zebra"
        Me.RdtZebra.UseVisualStyleBackColor = True
        '
        'Rdt246MP
        '
        Me.Rdt246MP.AutoSize = True
        Me.Rdt246MP.Checked = True
        Me.Rdt246MP.Location = New System.Drawing.Point(176, 109)
        Me.Rdt246MP.Name = "Rdt246MP"
        Me.Rdt246MP.Size = New System.Drawing.Size(95, 17)
        Me.Rdt246MP.TabIndex = 236
        Me.Rdt246MP.TabStop = True
        Me.Rdt246MP.Text = "TSC 246 MPro"
        Me.Rdt246MP.UseVisualStyleBackColor = True
        '
        'NetDefTextBox
        '
        Me.NetDefTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.NetDefTextBox.CustomButton.Image = Nothing
        Me.NetDefTextBox.CustomButton.Location = New System.Drawing.Point(32, 2)
        Me.NetDefTextBox.CustomButton.Name = ""
        Me.NetDefTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.NetDefTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.NetDefTextBox.CustomButton.TabIndex = 1
        Me.NetDefTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.NetDefTextBox.CustomButton.UseSelectable = True
        Me.NetDefTextBox.CustomButton.Visible = False
        Me.NetDefTextBox.Enabled = False
        Me.NetDefTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.NetDefTextBox.Lines = New String(-1) {}
        Me.NetDefTextBox.Location = New System.Drawing.Point(1109, 44)
        Me.NetDefTextBox.MaxLength = 32767
        Me.NetDefTextBox.Name = "NetDefTextBox"
        Me.NetDefTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.NetDefTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.NetDefTextBox.SelectedText = ""
        Me.NetDefTextBox.SelectionLength = 0
        Me.NetDefTextBox.SelectionStart = 0
        Me.NetDefTextBox.ShortcutsEnabled = True
        Me.NetDefTextBox.Size = New System.Drawing.Size(60, 30)
        Me.NetDefTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.NetDefTextBox.TabIndex = 238
        Me.NetDefTextBox.UseSelectable = True
        Me.NetDefTextBox.UseStyleColors = True
        Me.NetDefTextBox.Visible = False
        Me.NetDefTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.NetDefTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.Location = New System.Drawing.Point(1109, 22)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(50, 19)
        Me.MetroLabel10.TabIndex = 237
        Me.MetroLabel10.Text = "NetDef"
        Me.MetroLabel10.Visible = False
        '
        'BoxTareTextBox
        '
        Me.BoxTareTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.BoxTareTextBox.CustomButton.Image = Nothing
        Me.BoxTareTextBox.CustomButton.Location = New System.Drawing.Point(55, 2)
        Me.BoxTareTextBox.CustomButton.Name = ""
        Me.BoxTareTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.BoxTareTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.BoxTareTextBox.CustomButton.TabIndex = 1
        Me.BoxTareTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.BoxTareTextBox.CustomButton.UseSelectable = True
        Me.BoxTareTextBox.CustomButton.Visible = False
        Me.BoxTareTextBox.Enabled = False
        Me.BoxTareTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.BoxTareTextBox.Lines = New String(-1) {}
        Me.BoxTareTextBox.Location = New System.Drawing.Point(939, 109)
        Me.BoxTareTextBox.MaxLength = 32767
        Me.BoxTareTextBox.Name = "BoxTareTextBox"
        Me.BoxTareTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.BoxTareTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.BoxTareTextBox.SelectedText = ""
        Me.BoxTareTextBox.SelectionLength = 0
        Me.BoxTareTextBox.SelectionStart = 0
        Me.BoxTareTextBox.ShortcutsEnabled = True
        Me.BoxTareTextBox.Size = New System.Drawing.Size(83, 30)
        Me.BoxTareTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.BoxTareTextBox.TabIndex = 240
        Me.BoxTareTextBox.UseSelectable = True
        Me.BoxTareTextBox.UseStyleColors = True
        Me.BoxTareTextBox.Visible = False
        Me.BoxTareTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.BoxTareTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.Location = New System.Drawing.Point(941, 86)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(58, 19)
        Me.MetroLabel11.TabIndex = 239
        Me.MetroLabel11.Text = "Box tare"
        Me.MetroLabel11.Visible = False
        '
        'GrossDefTextBox
        '
        Me.GrossDefTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.GrossDefTextBox.CustomButton.Image = Nothing
        Me.GrossDefTextBox.CustomButton.Location = New System.Drawing.Point(46, 2)
        Me.GrossDefTextBox.CustomButton.Name = ""
        Me.GrossDefTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.GrossDefTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.GrossDefTextBox.CustomButton.TabIndex = 1
        Me.GrossDefTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.GrossDefTextBox.CustomButton.UseSelectable = True
        Me.GrossDefTextBox.CustomButton.Visible = False
        Me.GrossDefTextBox.Enabled = False
        Me.GrossDefTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.GrossDefTextBox.Lines = New String(-1) {}
        Me.GrossDefTextBox.Location = New System.Drawing.Point(1029, 45)
        Me.GrossDefTextBox.MaxLength = 32767
        Me.GrossDefTextBox.Name = "GrossDefTextBox"
        Me.GrossDefTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.GrossDefTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.GrossDefTextBox.SelectedText = ""
        Me.GrossDefTextBox.SelectionLength = 0
        Me.GrossDefTextBox.SelectionStart = 0
        Me.GrossDefTextBox.ShortcutsEnabled = True
        Me.GrossDefTextBox.Size = New System.Drawing.Size(74, 30)
        Me.GrossDefTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.GrossDefTextBox.TabIndex = 242
        Me.GrossDefTextBox.UseSelectable = True
        Me.GrossDefTextBox.UseStyleColors = True
        Me.GrossDefTextBox.Visible = False
        Me.GrossDefTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.GrossDefTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel12
        '
        Me.MetroLabel12.AutoSize = True
        Me.MetroLabel12.Location = New System.Drawing.Point(1023, 22)
        Me.MetroLabel12.Name = "MetroLabel12"
        Me.MetroLabel12.Size = New System.Drawing.Size(67, 19)
        Me.MetroLabel12.TabIndex = 241
        Me.MetroLabel12.Text = "Gross Def."
        Me.MetroLabel12.Visible = False
        '
        'BarcodeTextBox
        '
        Me.BarcodeTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.BarcodeTextBox.CustomButton.Image = Nothing
        Me.BarcodeTextBox.CustomButton.Location = New System.Drawing.Point(121, 2)
        Me.BarcodeTextBox.CustomButton.Name = ""
        Me.BarcodeTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.BarcodeTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.BarcodeTextBox.CustomButton.TabIndex = 1
        Me.BarcodeTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.BarcodeTextBox.CustomButton.UseSelectable = True
        Me.BarcodeTextBox.CustomButton.Visible = False
        Me.BarcodeTextBox.Enabled = False
        Me.BarcodeTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.BarcodeTextBox.Lines = New String(-1) {}
        Me.BarcodeTextBox.Location = New System.Drawing.Point(784, 108)
        Me.BarcodeTextBox.MaxLength = 32767
        Me.BarcodeTextBox.Name = "BarcodeTextBox"
        Me.BarcodeTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.BarcodeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.BarcodeTextBox.SelectedText = ""
        Me.BarcodeTextBox.SelectionLength = 0
        Me.BarcodeTextBox.SelectionStart = 0
        Me.BarcodeTextBox.ShortcutsEnabled = True
        Me.BarcodeTextBox.Size = New System.Drawing.Size(149, 30)
        Me.BarcodeTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.BarcodeTextBox.TabIndex = 244
        Me.BarcodeTextBox.UseSelectable = True
        Me.BarcodeTextBox.UseStyleColors = True
        Me.BarcodeTextBox.Visible = False
        Me.BarcodeTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.BarcodeTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel13
        '
        Me.MetroLabel13.AutoSize = True
        Me.MetroLabel13.Location = New System.Drawing.Point(785, 86)
        Me.MetroLabel13.Name = "MetroLabel13"
        Me.MetroLabel13.Size = New System.Drawing.Size(58, 19)
        Me.MetroLabel13.TabIndex = 243
        Me.MetroLabel13.Text = "Barcode"
        Me.MetroLabel13.Visible = False
        '
        'NetRealTextBox
        '
        Me.NetRealTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.NetRealTextBox.CustomButton.Image = Nothing
        Me.NetRealTextBox.CustomButton.Location = New System.Drawing.Point(46, 2)
        Me.NetRealTextBox.CustomButton.Name = ""
        Me.NetRealTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.NetRealTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.NetRealTextBox.CustomButton.TabIndex = 1
        Me.NetRealTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.NetRealTextBox.CustomButton.UseSelectable = True
        Me.NetRealTextBox.CustomButton.Visible = False
        Me.NetRealTextBox.Enabled = False
        Me.NetRealTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.NetRealTextBox.Lines = New String(-1) {}
        Me.NetRealTextBox.Location = New System.Drawing.Point(1029, 109)
        Me.NetRealTextBox.MaxLength = 32767
        Me.NetRealTextBox.Name = "NetRealTextBox"
        Me.NetRealTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.NetRealTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.NetRealTextBox.SelectedText = ""
        Me.NetRealTextBox.SelectionLength = 0
        Me.NetRealTextBox.SelectionStart = 0
        Me.NetRealTextBox.ShortcutsEnabled = True
        Me.NetRealTextBox.Size = New System.Drawing.Size(74, 30)
        Me.NetRealTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.NetRealTextBox.TabIndex = 246
        Me.NetRealTextBox.UseSelectable = True
        Me.NetRealTextBox.UseStyleColors = True
        Me.NetRealTextBox.Visible = False
        Me.NetRealTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.NetRealTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel14
        '
        Me.MetroLabel14.AutoSize = True
        Me.MetroLabel14.Location = New System.Drawing.Point(1023, 86)
        Me.MetroLabel14.Name = "MetroLabel14"
        Me.MetroLabel14.Size = New System.Drawing.Size(55, 19)
        Me.MetroLabel14.TabIndex = 245
        Me.MetroLabel14.Text = "NetReal"
        Me.MetroLabel14.Visible = False
        '
        'Sp_PD_SEL_REPRINTLABELTableAdapter1
        '
        Me.Sp_PD_SEL_REPRINTLABELTableAdapter1.ClearBeforeFill = True
        '
        'MetroGrid
        '
        Me.MetroGrid.AllowUserToAddRows = False
        Me.MetroGrid.AllowUserToDeleteRows = False
        Me.MetroGrid.AllowUserToResizeRows = False
        Me.MetroGrid.AutoGenerateColumns = False
        Me.MetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.MetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.GradersDataGridViewTextBoxColumn, Me.GradeDataGridViewTextBoxColumn, Me.BarcodeDataGridViewTextBoxColumn, Me.CasenoDataGridViewTextBoxColumn, Me.NetDataGridViewTextBoxColumn, Me.NetRealDataGridViewTextBoxColumn, Me.ProductionTypeDataGridViewTextBoxColumn, Me.BoxtareDataGridViewTextBoxColumn, Me.GrossdefDataGridViewTextBoxColumn, Me.GrossrealDataGridViewTextBoxColumn, Me.PdremarkDataGridViewTextBoxColumn, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.ToCustomer, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.packingdate, Me.packingtime})
        Me.MetroGrid.DataSource = Me.SpPDSELREPRINTLABELBindingSource1
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid.DefaultCellStyle = DataGridViewCellStyle8
        Me.MetroGrid.EnableHeadersVisualStyles = False
        Me.MetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.Location = New System.Drawing.Point(23, 145)
        Me.MetroGrid.Name = "MetroGrid"
        Me.MetroGrid.ReadOnly = True
        Me.MetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.MetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid.RowTemplate.Height = 24
        Me.MetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid.Size = New System.Drawing.Size(1227, 576)
        Me.MetroGrid.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroGrid.TabIndex = 248
        '
        'GradersDataGridViewTextBoxColumn
        '
        Me.GradersDataGridViewTextBoxColumn.DataPropertyName = "Graders"
        Me.GradersDataGridViewTextBoxColumn.HeaderText = "Graders"
        Me.GradersDataGridViewTextBoxColumn.Name = "GradersDataGridViewTextBoxColumn"
        Me.GradersDataGridViewTextBoxColumn.ReadOnly = True
        Me.GradersDataGridViewTextBoxColumn.Width = 79
        '
        'GradeDataGridViewTextBoxColumn
        '
        Me.GradeDataGridViewTextBoxColumn.DataPropertyName = "Grade"
        Me.GradeDataGridViewTextBoxColumn.HeaderText = "Grade"
        Me.GradeDataGridViewTextBoxColumn.Name = "GradeDataGridViewTextBoxColumn"
        Me.GradeDataGridViewTextBoxColumn.ReadOnly = True
        Me.GradeDataGridViewTextBoxColumn.Width = 68
        '
        'BarcodeDataGridViewTextBoxColumn
        '
        Me.BarcodeDataGridViewTextBoxColumn.DataPropertyName = "Barcode"
        Me.BarcodeDataGridViewTextBoxColumn.HeaderText = "STEC Barcode"
        Me.BarcodeDataGridViewTextBoxColumn.Name = "BarcodeDataGridViewTextBoxColumn"
        Me.BarcodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.BarcodeDataGridViewTextBoxColumn.Width = 113
        '
        'CasenoDataGridViewTextBoxColumn
        '
        Me.CasenoDataGridViewTextBoxColumn.DataPropertyName = "Caseno"
        Me.CasenoDataGridViewTextBoxColumn.HeaderText = "Caseno"
        Me.CasenoDataGridViewTextBoxColumn.Name = "CasenoDataGridViewTextBoxColumn"
        Me.CasenoDataGridViewTextBoxColumn.ReadOnly = True
        Me.CasenoDataGridViewTextBoxColumn.Width = 76
        '
        'NetDataGridViewTextBoxColumn
        '
        Me.NetDataGridViewTextBoxColumn.DataPropertyName = "Net"
        Me.NetDataGridViewTextBoxColumn.HeaderText = "Net"
        Me.NetDataGridViewTextBoxColumn.Name = "NetDataGridViewTextBoxColumn"
        Me.NetDataGridViewTextBoxColumn.ReadOnly = True
        Me.NetDataGridViewTextBoxColumn.Width = 54
        '
        'NetRealDataGridViewTextBoxColumn
        '
        Me.NetRealDataGridViewTextBoxColumn.DataPropertyName = "NetReal"
        Me.NetRealDataGridViewTextBoxColumn.HeaderText = "NetReal"
        Me.NetRealDataGridViewTextBoxColumn.Name = "NetRealDataGridViewTextBoxColumn"
        Me.NetRealDataGridViewTextBoxColumn.ReadOnly = True
        Me.NetRealDataGridViewTextBoxColumn.Width = 81
        '
        'ProductionTypeDataGridViewTextBoxColumn
        '
        Me.ProductionTypeDataGridViewTextBoxColumn.DataPropertyName = "ProductionType"
        Me.ProductionTypeDataGridViewTextBoxColumn.HeaderText = "ProductionType"
        Me.ProductionTypeDataGridViewTextBoxColumn.Name = "ProductionTypeDataGridViewTextBoxColumn"
        Me.ProductionTypeDataGridViewTextBoxColumn.ReadOnly = True
        Me.ProductionTypeDataGridViewTextBoxColumn.Width = 127
        '
        'BoxtareDataGridViewTextBoxColumn
        '
        Me.BoxtareDataGridViewTextBoxColumn.DataPropertyName = "boxtare"
        Me.BoxtareDataGridViewTextBoxColumn.HeaderText = "boxtare"
        Me.BoxtareDataGridViewTextBoxColumn.Name = "BoxtareDataGridViewTextBoxColumn"
        Me.BoxtareDataGridViewTextBoxColumn.ReadOnly = True
        Me.BoxtareDataGridViewTextBoxColumn.Width = 79
        '
        'GrossdefDataGridViewTextBoxColumn
        '
        Me.GrossdefDataGridViewTextBoxColumn.DataPropertyName = "grossdef"
        Me.GrossdefDataGridViewTextBoxColumn.HeaderText = "grossdef"
        Me.GrossdefDataGridViewTextBoxColumn.Name = "GrossdefDataGridViewTextBoxColumn"
        Me.GrossdefDataGridViewTextBoxColumn.ReadOnly = True
        Me.GrossdefDataGridViewTextBoxColumn.Width = 84
        '
        'GrossrealDataGridViewTextBoxColumn
        '
        Me.GrossrealDataGridViewTextBoxColumn.DataPropertyName = "grossreal"
        Me.GrossrealDataGridViewTextBoxColumn.HeaderText = "grossreal"
        Me.GrossrealDataGridViewTextBoxColumn.Name = "GrossrealDataGridViewTextBoxColumn"
        Me.GrossrealDataGridViewTextBoxColumn.ReadOnly = True
        Me.GrossrealDataGridViewTextBoxColumn.Width = 87
        '
        'PdremarkDataGridViewTextBoxColumn
        '
        Me.PdremarkDataGridViewTextBoxColumn.DataPropertyName = "pdremark"
        Me.PdremarkDataGridViewTextBoxColumn.HeaderText = "pdremark"
        Me.PdremarkDataGridViewTextBoxColumn.Name = "PdremarkDataGridViewTextBoxColumn"
        Me.PdremarkDataGridViewTextBoxColumn.ReadOnly = True
        Me.PdremarkDataGridViewTextBoxColumn.Width = 91
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "FromCusLotNo"
        Me.DataGridViewTextBoxColumn1.HeaderText = "FromCusLotNo"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 122
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "CusMoistureDefault"
        Me.DataGridViewTextBoxColumn2.HeaderText = "CusMoistureDefault"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 155
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "AccuRunno"
        Me.DataGridViewTextBoxColumn3.HeaderText = "AccuRunno"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "CaseRunno"
        Me.DataGridViewTextBoxColumn4.HeaderText = "CaseRunno"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "CusRunno"
        Me.DataGridViewTextBoxColumn5.HeaderText = "CusRunno"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 93
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "CusGradeCode"
        Me.DataGridViewTextBoxColumn6.HeaderText = "CusGradeCode"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 122
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "CusNetDefault"
        Me.DataGridViewTextBoxColumn7.HeaderText = "CusNetDefault"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 122
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "CusGrossDefault"
        Me.DataGridViewTextBoxColumn8.HeaderText = "CusGrossDefault"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 133
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "CusPrintFormatName"
        Me.DataGridViewTextBoxColumn9.HeaderText = "CusPrintFormatName"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 164
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "STATUSS"
        Me.DataGridViewTextBoxColumn10.HeaderText = "STATUSS"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 82
        '
        'ToCustomer
        '
        Me.ToCustomer.DataPropertyName = "ToCustomer"
        Me.ToCustomer.HeaderText = "ToCustomer"
        Me.ToCustomer.Name = "ToCustomer"
        Me.ToCustomer.ReadOnly = True
        Me.ToCustomer.Width = 105
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "BCCustomer"
        Me.DataGridViewTextBoxColumn11.HeaderText = "BCCustomer"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 107
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "taredef"
        Me.DataGridViewTextBoxColumn12.HeaderText = "taredef"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 77
        '
        'packingdate
        '
        Me.packingdate.DataPropertyName = "packingdate"
        Me.packingdate.HeaderText = "packingdate"
        Me.packingdate.Name = "packingdate"
        Me.packingdate.ReadOnly = True
        Me.packingdate.Width = 106
        '
        'packingtime
        '
        Me.packingtime.DataPropertyName = "packingtime"
        Me.packingtime.HeaderText = "packingtime"
        Me.packingtime.Name = "packingtime"
        Me.packingtime.ReadOnly = True
        Me.packingtime.Width = 106
        '
        'SpPDSELREPRINTLABELBindingSource1
        '
        Me.SpPDSELREPRINTLABELBindingSource1.DataMember = "sp_PD_SEL_REPRINTLABEL"
        Me.SpPDSELREPRINTLABELBindingSource1.DataSource = Me.ProcessingDataSet1
        '
        'TypeTableAdapter1
        '
        Me.TypeTableAdapter1.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_CropFromPackedgradeTableAdapter1
        '
        Me.Sp_Shipping_SEL_CropFromPackedgradeTableAdapter1.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter1
        '
        Me.Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter1.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_PackedgradeByGradersBindingSource
        '
        Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource.DataMember = "sp_Shipping_SEL_PackedgradeByGraders"
        Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource.DataSource = Me.ProcessingDataSet1
        '
        'Sp_Shipping_SEL_PackedgradeByGradersTableAdapter1
        '
        Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter1.ClearBeforeFill = True
        '
        'pd_ryo_customer_label_setupBindingSource
        '
        Me.pd_ryo_customer_label_setupBindingSource.DataMember = "pd_ryo_customer_label_setup"
        '
        'pd_ryo_customer_label_setupTableAdapter
        '
        Me.pd_ryo_customer_label_setupTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager1
        '
        Me.TableAdapterManager1.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager1.pd_ryo_customer_label_setupTableAdapter = Nothing
        Me.TableAdapterManager1.typeTableAdapter = Me.TypeTableAdapter1
        Me.TableAdapterManager1.UpdateOrder = PrintLabelPacking.ProcessingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Graders
        '
        Me.Graders.DataPropertyName = "Graders"
        Me.Graders.HeaderText = "Graders"
        Me.Graders.Name = "Graders"
        Me.Graders.ReadOnly = True
        Me.Graders.Width = 99
        '
        'Grade
        '
        Me.Grade.DataPropertyName = "Grade"
        Me.Grade.HeaderText = "Grade"
        Me.Grade.Name = "Grade"
        Me.Grade.ReadOnly = True
        Me.Grade.Width = 84
        '
        'Caseno
        '
        Me.Caseno.DataPropertyName = "Caseno"
        Me.Caseno.HeaderText = "Caseno"
        Me.Caseno.Name = "Caseno"
        Me.Caseno.ReadOnly = True
        Me.Caseno.Width = 93
        '
        'Net
        '
        Me.Net.DataPropertyName = "Net"
        Me.Net.HeaderText = "Net"
        Me.Net.Name = "Net"
        Me.Net.ReadOnly = True
        Me.Net.Width = 64
        '
        'NetReal
        '
        Me.NetReal.DataPropertyName = "NetReal"
        Me.NetReal.HeaderText = "NetReal"
        Me.NetReal.Name = "NetReal"
        Me.NetReal.ReadOnly = True
        Me.NetReal.Width = 96
        '
        'ProductionType
        '
        Me.ProductionType.DataPropertyName = "ProductionType"
        Me.ProductionType.HeaderText = "ProductionType"
        Me.ProductionType.Name = "ProductionType"
        Me.ProductionType.ReadOnly = True
        Me.ProductionType.Width = 159
        '
        'boxtare
        '
        Me.boxtare.DataPropertyName = "boxtare"
        Me.boxtare.HeaderText = "boxtare"
        Me.boxtare.Name = "boxtare"
        Me.boxtare.ReadOnly = True
        Me.boxtare.Width = 96
        '
        'grossdef
        '
        Me.grossdef.DataPropertyName = "grossdef"
        Me.grossdef.HeaderText = "grossdef"
        Me.grossdef.Name = "grossdef"
        Me.grossdef.ReadOnly = True
        Me.grossdef.Width = 104
        '
        'grossreal
        '
        Me.grossreal.DataPropertyName = "grossreal"
        Me.grossreal.HeaderText = "grossreal"
        Me.grossreal.Name = "grossreal"
        Me.grossreal.ReadOnly = True
        Me.grossreal.Width = 108
        '
        'pdremark
        '
        Me.pdremark.DataPropertyName = "pdremark"
        Me.pdremark.HeaderText = "pdremark"
        Me.pdremark.Name = "pdremark"
        Me.pdremark.ReadOnly = True
        Me.pdremark.Width = 113
        '
        'fromcuslotno
        '
        Me.fromcuslotno.DataPropertyName = "fromcuslotno"
        Me.fromcuslotno.HeaderText = "fromcuslotno"
        Me.fromcuslotno.Name = "fromcuslotno"
        Me.fromcuslotno.ReadOnly = True
        Me.fromcuslotno.Width = 141
        '
        'CUSMOISTUREDEFAULT
        '
        Me.CUSMOISTUREDEFAULT.DataPropertyName = "CUSMOISTUREDEFAULT"
        Me.CUSMOISTUREDEFAULT.HeaderText = "CUSMOISTUREDEFAULT"
        Me.CUSMOISTUREDEFAULT.Name = "CUSMOISTUREDEFAULT"
        Me.CUSMOISTUREDEFAULT.ReadOnly = True
        Me.CUSMOISTUREDEFAULT.Width = 220
        '
        'accurunno
        '
        Me.accurunno.DataPropertyName = "accurunno"
        Me.accurunno.HeaderText = "accurunno"
        Me.accurunno.Name = "accurunno"
        Me.accurunno.ReadOnly = True
        Me.accurunno.Width = 119
        '
        'caserunno
        '
        Me.caserunno.DataPropertyName = "caserunno"
        Me.caserunno.HeaderText = "caserunno"
        Me.caserunno.Name = "caserunno"
        Me.caserunno.ReadOnly = True
        Me.caserunno.Width = 118
        '
        'cusrunno
        '
        Me.cusrunno.DataPropertyName = "cusrunno"
        Me.cusrunno.HeaderText = "cusrunno"
        Me.cusrunno.Name = "cusrunno"
        Me.cusrunno.ReadOnly = True
        Me.cusrunno.Width = 110
        '
        'CUSGRADECODE
        '
        Me.CUSGRADECODE.DataPropertyName = "CUSGRADECODE"
        Me.CUSGRADECODE.HeaderText = "CUSGRADECODE"
        Me.CUSGRADECODE.Name = "CUSGRADECODE"
        Me.CUSGRADECODE.ReadOnly = True
        Me.CUSGRADECODE.Width = 166
        '
        'CUSNETDEFAULT
        '
        Me.CUSNETDEFAULT.DataPropertyName = "CUSNETDEFAULT"
        Me.CUSNETDEFAULT.HeaderText = "CUSNETDEFAULT"
        Me.CUSNETDEFAULT.Name = "CUSNETDEFAULT"
        Me.CUSNETDEFAULT.ReadOnly = True
        Me.CUSNETDEFAULT.Width = 166
        '
        'CUSGROSSDEFAULT
        '
        Me.CUSGROSSDEFAULT.DataPropertyName = "CUSGROSSDEFAULT"
        Me.CUSGROSSDEFAULT.HeaderText = "CUSGROSSDEFAULT"
        Me.CUSGROSSDEFAULT.Name = "CUSGROSSDEFAULT"
        Me.CUSGROSSDEFAULT.ReadOnly = True
        Me.CUSGROSSDEFAULT.Width = 189
        '
        'CUSPRINTFORMATNAME
        '
        Me.CUSPRINTFORMATNAME.DataPropertyName = "CUSPRINTFORMATNAME"
        Me.CUSPRINTFORMATNAME.HeaderText = "CUSPRINTFORMATNAME"
        Me.CUSPRINTFORMATNAME.Name = "CUSPRINTFORMATNAME"
        Me.CUSPRINTFORMATNAME.ReadOnly = True
        Me.CUSPRINTFORMATNAME.Width = 230
        '
        'STATUSS
        '
        Me.STATUSS.DataPropertyName = "STATUSS"
        Me.STATUSS.HeaderText = "STATUSS"
        Me.STATUSS.Name = "STATUSS"
        Me.STATUSS.ReadOnly = True
        Me.STATUSS.Width = 103
        '
        'customer
        '
        Me.customer.DataPropertyName = "customer"
        Me.customer.HeaderText = "customer"
        Me.customer.Name = "customer"
        Me.customer.ReadOnly = True
        Me.customer.Width = 110
        '
        'BCCustomer
        '
        Me.BCCustomer.DataPropertyName = "BCCustomer"
        Me.BCCustomer.HeaderText = "BCCustomer"
        Me.BCCustomer.Name = "BCCustomer"
        Me.BCCustomer.ReadOnly = True
        Me.BCCustomer.Width = 132
        '
        'taredef
        '
        Me.taredef.DataPropertyName = "taredef"
        Me.taredef.HeaderText = "taredef"
        Me.taredef.Name = "taredef"
        Me.taredef.ReadOnly = True
        Me.taredef.Width = 93
        '
        'CountOfLabel
        '
        Me.CountOfLabel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.CountOfLabel.CustomButton.Image = Nothing
        Me.CountOfLabel.CustomButton.Location = New System.Drawing.Point(46, 2)
        Me.CountOfLabel.CustomButton.Name = ""
        Me.CountOfLabel.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.CountOfLabel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CountOfLabel.CustomButton.TabIndex = 1
        Me.CountOfLabel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CountOfLabel.CustomButton.UseSelectable = True
        Me.CountOfLabel.CustomButton.Visible = False
        Me.CountOfLabel.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CountOfLabel.Lines = New String(-1) {}
        Me.CountOfLabel.Location = New System.Drawing.Point(600, 68)
        Me.CountOfLabel.MaxLength = 32767
        Me.CountOfLabel.Name = "CountOfLabel"
        Me.CountOfLabel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CountOfLabel.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CountOfLabel.SelectedText = ""
        Me.CountOfLabel.SelectionLength = 0
        Me.CountOfLabel.SelectionStart = 0
        Me.CountOfLabel.ShortcutsEnabled = True
        Me.CountOfLabel.Size = New System.Drawing.Size(74, 30)
        Me.CountOfLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CountOfLabel.TabIndex = 250
        Me.CountOfLabel.UseSelectable = True
        Me.CountOfLabel.UseStyleColors = True
        Me.CountOfLabel.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CountOfLabel.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel15
        '
        Me.MetroLabel15.AutoSize = True
        Me.MetroLabel15.Location = New System.Drawing.Point(508, 71)
        Me.MetroLabel15.Name = "MetroLabel15"
        Me.MetroLabel15.Size = New System.Drawing.Size(83, 19)
        Me.MetroLabel15.TabIndex = 249
        Me.MetroLabel15.Text = "No. of labels"
        '
        'Rdt247
        '
        Me.Rdt247.AutoSize = True
        Me.Rdt247.Location = New System.Drawing.Point(486, 109)
        Me.Rdt247.Name = "Rdt247"
        Me.Rdt247.Size = New System.Drawing.Size(91, 17)
        Me.Rdt247.TabIndex = 251
        Me.Rdt247.TabStop = True
        Me.Rdt247.Text = "TSC TTP-247"
        Me.Rdt247.UseVisualStyleBackColor = True
        '
        'Rdt384MT
        '
        Me.Rdt384MT.AutoSize = True
        Me.Rdt384MT.Location = New System.Drawing.Point(600, 109)
        Me.Rdt384MT.Name = "Rdt384MT"
        Me.Rdt384MT.Size = New System.Drawing.Size(86, 17)
        Me.Rdt384MT.TabIndex = 252
        Me.Rdt384MT.Text = "TSC 384 MT"
        Me.Rdt384MT.UseVisualStyleBackColor = True
        '
        'FrmPrintLabel
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1328, 759)
        Me.Controls.Add(Me.Rdt384MT)
        Me.Controls.Add(Me.Rdt247)
        Me.Controls.Add(Me.CountOfLabel)
        Me.Controls.Add(Me.MetroLabel15)
        Me.Controls.Add(Me.MetroGrid)
        Me.Controls.Add(Me.NetRealTextBox)
        Me.Controls.Add(Me.MetroLabel14)
        Me.Controls.Add(Me.BarcodeTextBox)
        Me.Controls.Add(Me.MetroLabel13)
        Me.Controls.Add(Me.GrossDefTextBox)
        Me.Controls.Add(Me.MetroLabel12)
        Me.Controls.Add(Me.BoxTareTextBox)
        Me.Controls.Add(Me.MetroLabel11)
        Me.Controls.Add(Me.NetDefTextBox)
        Me.Controls.Add(Me.MetroLabel10)
        Me.Controls.Add(Me.Rdt246MP)
        Me.Controls.Add(Me.RdtZebra)
        Me.Controls.Add(Me.Rdt384M)
        Me.Controls.Add(Me.ChkRENew)
        Me.Controls.Add(Me.CaseNoTextBox)
        Me.Controls.Add(Me.GradeTextBox)
        Me.Controls.Add(Me.MetroLabel8)
        Me.Controls.Add(Me.MetroLabel9)
        Me.Controls.Add(Me.CropComboBox)
        Me.Controls.Add(Me.TypeComboBox)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.MetroLabel7)
        Me.Controls.Add(Me.MetroLabel6)
        Me.Controls.Add(Me.PackedGradeComboBox)
        Me.Controls.Add(Me.MetroLabel4)
        Me.Controls.Add(Me.MetroTile1)
        Me.Controls.Add(Me.PackgradeLabel)
        Me.Controls.Add(Me.CasenoLabel)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.MetroLabel24)
        Me.Controls.Add(Me.PrintTile)
        Me.Controls.Add(Me.BCTextbox)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Name = "FrmPrintLabel"
        Me.Style = MetroFramework.MetroColorStyle.Brown
        Me.Text = "Print label from packing"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.SpShippingSELPackedgradeByInternalGradeBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcessingDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELPackedgradeByInternalGradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpPDSELREPRINTLABELBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELPackedgradeByGradersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Packing_GET_DataByBCBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELCropFromPackedgradeBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELCropFromPackedgradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TypeBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_PD_SEL_REPRINTLABELBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpPDSELREPRINTLABELBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pd_ryo_customer_label_setupBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BCTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents PrintTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel24 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ProcessingDataSet As ProcessingDataSet
    Friend WithEvents Sp_Packing_GET_DataByBCBindingSource As BindingSource
    Friend WithEvents Sp_Packing_GET_DataByBCTableAdapter As ProcessingDataSetTableAdapters.sp_Packing_GET_DataByBCTableAdapter
    Friend WithEvents pd_ryo_customer_label_setupBindingSource As BindingSource
    Friend WithEvents pd_ryo_customer_label_setupTableAdapter As ProcessingDataSetTableAdapters.pd_ryo_customer_label_setupTableAdapter
    Friend WithEvents TableAdapterManager As ProcessingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CasenoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents PackgradeLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents PackedGradeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents SpShippingSELPackedgradeByGradersBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PackedgradeByGradersTableAdapter As ProcessingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByGradersTableAdapter
    Friend WithEvents CropComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents TypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents SpShippingSELCropFromPackedgradeBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_CropFromPackedgradeTableAdapter As ProcessingDataSetTableAdapters.sp_Shipping_SEL_CropFromPackedgradeTableAdapter
    Friend WithEvents TypeBindingSource As BindingSource
    Friend WithEvents TypeTableAdapter As ProcessingDataSetTableAdapters.typeTableAdapter
    Friend WithEvents SpShippingSELPackedgradeByInternalGradeBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter As ProcessingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents GradeTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents CaseNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents ChkRENew As CheckBox
    Friend WithEvents SpPDSELREPRINTLABELBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_PD_SEL_REPRINTLABELTableAdapter As PrintLabelPacking.ProcessingDataSetTableAdapters.sp_PD_SEL_REPRINTLABELTableAdapter
    Friend WithEvents Rdt384M As System.Windows.Forms.RadioButton
    Friend WithEvents RdtZebra As System.Windows.Forms.RadioButton
    Friend WithEvents Rdt246MP As System.Windows.Forms.RadioButton
    Friend WithEvents NetDefTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BoxTareTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents GrossDefTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel12 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BarcodeTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel13 As MetroFramework.Controls.MetroLabel
    Friend WithEvents NetRealTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel14 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_PD_SEL_REPRINTLABELBindingSource As BindingSource
    Friend WithEvents Sp_PD_SEL_REPRINTLABELTableAdapter1 As ProcessingDataSetTableAdapters.sp_PD_SEL_REPRINTLABELTableAdapter
    Friend WithEvents MetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents ProcessingDataSet1 As ProcessingDataSet
    Friend WithEvents TypeBindingSource1 As BindingSource
    Friend WithEvents TypeTableAdapter1 As ProcessingDataSetTableAdapters.typeTableAdapter
    Friend WithEvents SpShippingSELCropFromPackedgradeBindingSource1 As BindingSource
    Friend WithEvents Sp_Shipping_SEL_CropFromPackedgradeTableAdapter1 As ProcessingDataSetTableAdapters.sp_Shipping_SEL_CropFromPackedgradeTableAdapter
    Friend WithEvents SpShippingSELPackedgradeByInternalGradeBindingSource1 As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter1 As ProcessingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter
    Friend WithEvents Sp_Shipping_SEL_PackedgradeByGradersBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PackedgradeByGradersTableAdapter1 As ProcessingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByGradersTableAdapter
    Friend WithEvents TableAdapterManager1 As ProcessingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents SpPDSELREPRINTLABELBindingSource1 As BindingSource
    Friend WithEvents Graders As DataGridViewTextBoxColumn
    Friend WithEvents Grade As DataGridViewTextBoxColumn
    'Friend WithEvents Barcode As DataGridViewTextBoxColumn
    Friend WithEvents Caseno As DataGridViewTextBoxColumn
    Friend WithEvents Net As DataGridViewTextBoxColumn
    Friend WithEvents NetReal As DataGridViewTextBoxColumn
    Friend WithEvents ProductionType As DataGridViewTextBoxColumn
    Friend WithEvents boxtare As DataGridViewTextBoxColumn
    Friend WithEvents grossdef As DataGridViewTextBoxColumn
    Friend WithEvents grossreal As DataGridViewTextBoxColumn
    Friend WithEvents pdremark As DataGridViewTextBoxColumn
    Friend WithEvents fromcuslotno As DataGridViewTextBoxColumn
    Friend WithEvents CUSMOISTUREDEFAULT As DataGridViewTextBoxColumn
    Friend WithEvents accurunno As DataGridViewTextBoxColumn
    Friend WithEvents caserunno As DataGridViewTextBoxColumn
    Friend WithEvents cusrunno As DataGridViewTextBoxColumn
    Friend WithEvents CUSGRADECODE As DataGridViewTextBoxColumn
    Friend WithEvents CUSNETDEFAULT As DataGridViewTextBoxColumn
    Friend WithEvents CUSGROSSDEFAULT As DataGridViewTextBoxColumn
    Friend WithEvents CUSPRINTFORMATNAME As DataGridViewTextBoxColumn
    Friend WithEvents STATUSS As DataGridViewTextBoxColumn
    Friend WithEvents customer As DataGridViewTextBoxColumn
    Friend WithEvents BCCustomer As DataGridViewTextBoxColumn
    Friend WithEvents taredef As DataGridViewTextBoxColumn
    Friend WithEvents CountOfLabel As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel15 As MetroFramework.Controls.MetroLabel
    Friend WithEvents GradersDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GradeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BarcodeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CasenoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetRealDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductionTypeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BoxtareDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GrossdefDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GrossrealDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PdremarkDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As DataGridViewTextBoxColumn
    Friend WithEvents ToCustomer As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As DataGridViewTextBoxColumn
    Friend WithEvents packingdate As DataGridViewTextBoxColumn
    Friend WithEvents packingtime As DataGridViewTextBoxColumn
    Friend WithEvents Rdt247 As RadioButton
    Friend WithEvents Rdt384MT As RadioButton
End Class
