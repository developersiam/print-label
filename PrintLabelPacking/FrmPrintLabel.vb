﻿Public Class FrmPrintLabel
    Inherits MetroFramework.Forms.MetroForm

    'module Barcode Sticker
    Declare Sub openport Lib "C:\windows\system\tsclib.dll" (ByVal PrinterName As String)
    Declare Sub closeport Lib "C:\windows\system\tsclib.dll" ()
    Declare Sub sendcommand Lib "C:\windows\system\tsclib.dll" (ByVal command As String)
    Declare Sub setup Lib "C:\windows\system\tsclib.dll" (ByVal LabelWidth As String, ByVal LabelHeight As String, ByVal Speed As String, ByVal Density As String, ByVal Sensor As String, ByVal Vertical As String, ByVal Offset As String)
    Declare Sub downloadpcx Lib "C:\windows\system\tsclib.dll" (ByVal Filename As String, ByVal ImageName As String)
    Declare Sub barcode Lib "C:\windows\system\tsclib.dll" (ByVal X As String, ByVal Y As String, ByVal CodeType As String, ByVal Height As String, ByVal Readable As String, ByVal rotation As String, ByVal Narrow As String, ByVal Wide As String, ByVal Code As String)
    Declare Sub printerfont Lib "C:\windows\system\tsclib.dll" (ByVal X As String, ByVal Y As String, ByVal FontName As String, ByVal rotation As String, ByVal Xmul As String, ByVal Ymul As String, ByVal Content As String)
    Declare Sub clearbuffer Lib "C:\windows\system\tsclib.dll" ()
    Declare Sub printlabel Lib "C:\windows\system\tsclib.dll" (ByVal NumberOfSet As String, ByVal NumberOfCopy As String)
    Declare Sub formfeed Lib "C:\windows\system\tsclib.dll" ()
    Declare Sub nobackfeed Lib "C:\windows\system\tsclib.dll" ()
    Declare Sub windowsfont Lib "C:\windows\system\tsclib.dll" (ByVal X As Integer, ByVal Y As Integer, ByVal fontheight As Integer, ByVal rotation As Integer, ByVal fontstyle As Integer, ByVal fontunderline As Integer, ByVal FaceName As String, ByVal TextContent As String)

    Dim minCase = 0
    Dim maxCase = 0
    Dim printCopy = 2
    Dim printerName = ""

    Private Sub FrmPrintLabel_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1321, 759)
            Me.TypeTableAdapter1.Fill(Me.ProcessingDataSet1.type)
            Me.Sp_Shipping_SEL_CropFromPackedgradeTableAdapter1.Fill(Me.ProcessingDataSet1.sp_Shipping_SEL_CropFromPackedgrade)
            CountOfLabel.Text = 2 'Default of print copies.
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub PrintTile_Click(sender As Object, e As EventArgs) Handles PrintTile.Click
        Try
            If String.IsNullOrEmpty(CountOfLabel.Text) Then
                MessageBox.Show("กรุณา ระบุจำนวนแผ่นที่จะพิมพ์", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If
            Dim templatePath As String = "\\192.168.0.221\Log_setting\Program_2015\Processing\new version\Packing\CustomerFormat\"
            'Dim templatePath As String = "C:\CustomerFormatForZebra\"
            Dim SelectedRowCount As Integer
            SelectedRowCount = MetroGrid.Rows.GetRowCount(DataGridViewElementStates.Selected)
            If SelectedRowCount <= 0 Then
                MessageBox.Show("กรุณา Click เลือกแถวที่ต้องการจากตาราง", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Dim printInfoDataTable = New ProcessingDataSet.PDCustomerPackedPrintByCaseNoDataTable
            Dim printInfoTableAdapter = New ProcessingDataSetTableAdapters.PDCustomerPackedPrintByCaseNoTableAdapter
            Dim printInfoRow As ProcessingDataSet.PDCustomerPackedPrintByCaseNoRow

            If (Rdt246MP.Checked = True) Then
                printerName = "TSC TTP-246M Pro"
            ElseIf (RdtZebra.Checked = True) Then
                printerName = "ZDesigner 220Xi4 203 dpi"
            ElseIf (Rdt384M.Checked = True) Then
                printerName = "TSC TTP-384M"
            ElseIf (Rdt247.Checked = True) Then
                printerName = "TSC TTP-247"
            ElseIf (Rdt384MT.Checked = True) Then
                printerName = "TSC TTP-384MT"
            End If

            If ChkRENew.Checked = False Then
                Dim result As DialogResult
                result = MessageBox.Show("ต้องการปรินส์บาร์โค้ดจำนวน" & SelectedRowCount & " บาร์โค้ด ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If result = Windows.Forms.DialogResult.Yes Then
                    If (SelectedRowCount > 0) Then
                        For i As Integer = 0 To SelectedRowCount - 1
                            Dim grade = MetroGrid.SelectedRows(i).Cells(1).Value
                            Dim STECBarcode = MetroGrid.SelectedRows(i).Cells(2).Value.ToString()
                            Dim caseno = MetroGrid.SelectedRows(i).Cells(3).Value
                            Dim netdef = MetroGrid.SelectedRows(i).Cells(4).Value
                            Dim netreal = MetroGrid.SelectedRows(i).Cells(5).Value
                            Dim boxtare = MetroGrid.SelectedRows(i).Cells(7).Value
                            Dim grossdef = MetroGrid.SelectedRows(i).Cells(8).Value
                            Dim grossreal = MetroGrid.SelectedRows(i).Cells(9).Value
                            Dim pdremark = MetroGrid.SelectedRows(i).Cells(10).Value.ToString()
                            Dim fromCusLotNo = MetroGrid.SelectedRows(i).Cells(11).Value.ToString()
                            Dim moisture = MetroGrid.SelectedRows(i).Cells(12).Value
                            Dim accuRunno = MetroGrid.SelectedRows(i).Cells(13).Value
                            Dim caseRunno = MetroGrid.SelectedRows(i).Cells(14).Value
                            Dim cusRunno = MetroGrid.SelectedRows(i).Cells(15).Value
                            Dim cusGradeCode = MetroGrid.SelectedRows(i).Cells(16).Value.ToString()
                            Dim cusPrintFormatName = MetroGrid.SelectedRows(i).Cells(19).Value.ToString()
                            Dim customer = MetroGrid.SelectedRows(i).Cells(21).Value
                            Dim customerBarcode = MetroGrid.SelectedRows(i).Cells(22).Value.ToString()
                            Dim taredef = MetroGrid.SelectedRows(i).Cells(23).Value
                            Dim packingdate = DateTime.Parse(MetroGrid.SelectedRows(i).Cells(24).Value).ToString("dd/MM/yy")
                            Dim packingtime = MetroGrid.SelectedRows(i).Cells(25).Value

                            '=============================================================================
                            'Print customer barcode with Bartender template.
                            '=============================================================================
                            If Not String.IsNullOrEmpty(fromCusLotNo) Then
                                Dim btApp As BarTender.Application
                                Dim btFormat As BarTender.Format
                                btApp = New BarTender.Application
                                'btApp.Visible = True
                                btFormat = btApp.Formats.Open(templatePath + Trim(cusPrintFormatName) + ".btw", False, printerName)

                                '=============================================================================
                                'Get a printCopy from PDCustomerPackedPrint. (find by caseno and grade)
                                'Store เดิมไม่มี column print copy หากแก้ store เดิมต้องได้แก้โค้ตเยอะ
                                'เลยสร้าง Table Adaptor ใหม่แทนไปก่อนเพื่อใช้ชั่วคราว
                                '=============================================================================
                                If Not (caseno >= minCase And caseno <= maxCase) Then
                                    printInfoTableAdapter.Fill(printInfoDataTable, grade, caseno)
                                    If printInfoDataTable.Rows.Count() < 1 Then
                                        btFormat.Close(BarTender.BtSaveOptions.btDoNotSaveChanges)
                                        btApp.Quit(BarTender.BtSaveOptions.btDoNotSaveChanges)
                                        btFormat = Nothing
                                        btApp = Nothing
                                        Throw New ArgumentException("ไม่พบข้อมูลการตั้งค่าการพิมพ์บาร์โค้ต caseno " + caseno + " นี้ในระบบ ")
                                    End If
                                    printInfoRow = printInfoDataTable.Rows(0)
                                    minCase = printInfoDataTable.Rows(0).Item("MinCase")
                                    maxCase = printInfoDataTable.Rows(0).Item("MaxCase")
                                    printCopy = printInfoDataTable.Rows(0).Item("PrintCopy")
                                    'CountOfLabel.Text = printCopy
                                End If
                                '=============================================================================
                                '=============================================================================

                                If customer = "LTL" Then
                                    btFormat.SetNamedSubStringValue("LTLBarcode2", customerBarcode)
                                    btFormat.SetNamedSubStringValue("LTLGrade2", cusGradeCode)
                                    btFormat.SetNamedSubStringValue("NetDef2", Convert.ToDecimal(netdef).ToString("N0") + " Kg")
                                    btFormat.SetNamedSubStringValue("STECBarcode2", STECBarcode)
                                ElseIf customer = "ITG" Or
                                    Microsoft.VisualBasic.Left(cusPrintFormatName, 12) = "17-X4L_1-990" Then
                                    btFormat.SetNamedSubStringValue("Accumulative", accuRunno)
                                    btFormat.SetNamedSubStringValue("DealerBarCode", STECBarcode)
                                ElseIf customer = "PMI" Then
                                    btFormat.SetNamedSubStringValue("Lot", fromCusLotNo)
                                    btFormat.SetNamedSubStringValue("Accumulative", accuRunno)
                                    btFormat.SetNamedSubStringValue("CaseNo", caseRunno)
                                    btFormat.SetNamedSubStringValue("Run", cusRunno)
                                    btFormat.SetNamedSubStringValue("Moist", moisture)
                                    btFormat.SetNamedSubStringValue("GradeCode", cusGradeCode)
                                    btFormat.SetNamedSubStringValue("DealerBarCode", STECBarcode)
                                    btFormat.SetNamedSubStringValue("Net", Convert.ToString(netdef))
                                    btFormat.SetNamedSubStringValue("Gross", Convert.ToString(grossdef))
                                ElseIf (Microsoft.VisualBasic.Left(cusPrintFormatName, 7) = "KTG2018") Then
                                    btFormat.SetNamedSubStringValue("Grade", fromCusLotNo)
                                    btFormat.SetNamedSubStringValue("ACCUMULATIVE", accuRunno)
                                    btFormat.SetNamedSubStringValue("CUSGRADECODE", cusGradeCode)
                                    btFormat.SetNamedSubStringValue("STECBARCODE", STECBarcode)
                                    btFormat.SetNamedSubStringValue("GROSSNET", Convert.ToString(grossdef))
                                    btFormat.SetNamedSubStringValue("TARENET", Convert.ToString(taredef))
                                    btFormat.SetNamedSubStringValue("Net", Convert.ToString(netdef))
                                ElseIf customer = "JMC" And grade.Substring(3, 3) = "47P" Then
                                    Dim crop = CropComboBox.Text
                                    Dim batchID = "47P02020F"
                                    Dim material = "JL47P"
                                    Dim cusLotNo = "4700000263"
                                    Dim gross = 430
                                    Dim tare = 33
                                    Dim net = 397
                                    Dim type = "BU"
                                    Dim datecode = Microsoft.VisualBasic.Right("0" & Microsoft.VisualBasic.Day(packingdate), 2) +
                                        Microsoft.VisualBasic.Right("0" & Microsoft.VisualBasic.Month(packingdate), 2) +
                                        Microsoft.VisualBasic.Right("0" & Microsoft.VisualBasic.Year(packingdate), 2)
                                    Dim tmpTime = Replace(packingtime, ":", "").Substring(0, 4)
                                    Dim bcReserve = "0000" + batchID + "000TH039700" + material + caseRunno.ToString("D5")
                                    Dim QRCode = batchID + "," +
                                        cusLotNo + "," +
                                        crop + "," +
                                        material + "," +
                                        caseRunno.ToString("D5") + ",TH,SSTK," +
                                        crop + "," +
                                        cusRunno.ToString("D3") + "," +
                                        tmpTime + "," +
                                        datecode + "," +
                                        gross + "," +
                                        tare + "," +
                                        net + "," +
                                        type
                                    btFormat.SetNamedSubStringValue("CaseRunning", caseRunno)
                                    btFormat.SetNamedSubStringValue("CustomerBarcode2", bcReserve)
                                    btFormat.SetNamedSubStringValue("BCReserved", bcReserve)
                                    btFormat.SetNamedSubStringValue("Material", material)
                                    btFormat.SetNamedSubStringValue("STECBARCODE", STECBarcode)
                                    btFormat.SetNamedSubStringValue("QRCode", QRCode)
                                ElseIf customer = "JMC" And grade.Substring(3, 3) = "BMX" Then
                                    Dim crop = CropComboBox.Text
                                    Dim batchID = "BMX02022B"
                                    Dim material = "JLBMX"
                                    Dim cusLotNo = "4700000262"
                                    Dim gross = 430
                                    Dim tare = 33
                                    Dim net = 397
                                    Dim type = "BU"
                                    Dim datecode = Microsoft.VisualBasic.Right("0" & Microsoft.VisualBasic.Day(packingdate), 2) +
                                        Microsoft.VisualBasic.Right("0" & Microsoft.VisualBasic.Month(packingdate), 2) +
                                        Microsoft.VisualBasic.Right("0" & Microsoft.VisualBasic.Year(packingdate), 2)
                                    Dim tmpTime = Replace(packingtime, ":", "").Substring(0, 4)
                                    Dim bcReserve = "0000" + batchID + "000TH039700" + material + caseRunno.ToString("D5")
                                    Dim QRCode = batchID + "," +
                                        cusLotNo + "," +
                                        crop + "," +
                                        material + "," +
                                        caseRunno.ToString("D5") + ",TH,SSTK," +
                                        crop + "," +
                                        cusRunno.ToString("D3") + "," +
                                        tmpTime + "," +
                                        datecode + "," +
                                        gross + "," +
                                        tare + "," +
                                        net + "," +
                                        type
                                    btFormat.SetNamedSubStringValue("CaseRunning", caseRunno.ToString("D4"))
                                    btFormat.SetNamedSubStringValue("CustomerBarcode2", bcReserve)
                                    btFormat.SetNamedSubStringValue("BCReserved", bcReserve)
                                    btFormat.SetNamedSubStringValue("Material", material)
                                    btFormat.SetNamedSubStringValue("STECBARCODE", STECBarcode)
                                    btFormat.SetNamedSubStringValue("QRCode", QRCode)
                                End If

                                btFormat.IdenticalCopiesOfLabel = CountOfLabel.Text
                                btFormat.PrintOut(False, False)
                                btFormat.Close(BarTender.BtSaveOptions.btDoNotSaveChanges)
                                btApp.Quit(BarTender.BtSaveOptions.btDoNotSaveChanges)
                                btFormat = Nothing
                                btApp = Nothing
                                '=============================================================================
                                '=============================================================================
                            Else
                                Call openport(printerName)
                                '-------------- CHAMP/SUA/CHAORAI/ETC.. --------------
                                If grade.Length >= 8 Or grade.Length <= 6 Then
                                    If grade.Contains("CHAMP") = True Or grade.Contains("SUA") = True Then
                                        PrintBarcodeChamp(STECBarcode, caseno, packingdate)
                                        Exit Sub
                                    End If
                                End If

                                '=============================================================================
                                'Print Internal barcode.
                                '=============================================================================
                                Call setup("103", "76", "2.0", "1", "0", "0", "0")
                                Call sendcommand("SIZE 101.6 mm, 76.2 mm")
                                Call sendcommand("GAP  3 mm,0")
                                Call sendcommand("DIRECTION 1")
                                Call clearbuffer()
                                Call sendcommand("BAR 50,25,4,510")
                                Call sendcommand("BAR 820,25,4,510")
                                Call sendcommand("BAR 150,210,4,325")
                                Call sendcommand("BAR 50,25,770,4")

                                Select Case Len(Trim(grade))
                                    Case 4
                                        Call windowsfont(300, 50, 140, 0, 2, 0, "arial", Trim(grade)) '14-A
                                    Case 5
                                        Call windowsfont(250, 50, 140, 0, 2, 0, "arial", Trim(grade)) '14-AB
                                    Case 6
                                        Call windowsfont(220, 50, 140, 0, 2, 0, "arial", Trim(grade)) '14-ABC
                                    Case 7
                                        Call windowsfont(180, 50, 140, 0, 2, 0, "arial", Trim(grade)) '14-ABCD
                                    Case 8
                                        Call windowsfont(140, 50, 140, 0, 2, 0, "arial", Trim(grade)) '14-ABCDE
                                    Case 9
                                        Call windowsfont(100, 50, 140, 0, 2, 0, "arial", Trim(grade)) '14-ABCDEF
                                    Case 10
                                        Call windowsfont(70, 60, 120, 0, 2, 0, "arial", Trim(grade)) '14-ABCDEFG
                                    Case 12
                                        Call windowsfont(70, 60, 100, 0, 2, 0, "arial", Trim(grade)) '
                                    Case Else
                                        Call windowsfont(60, 60, 90, 0, 2, 0, "arial", Trim(grade)) '
                                End Select

                                Call barcode("60", "520", "128", "60", "0", "270", "2", "2", Trim(STECBarcode)) 'barcode µÑé§
                                Call windowsfont(120, 460, 30, 90, 0, 0, "arial", Trim(STECBarcode))
                                Call sendcommand("BAR 50,210,770,4")

                                Select Case Len(Trim(caseno))
                                    Case 1
                                        Call windowsfont(440, 212, 110, 0, 2, 0, "arial", Trim(caseno))   '1
                                    Case 2
                                        Call windowsfont(420, 212, 110, 0, 2, 0, "arial", Trim(caseno))   '12
                                    Case 3
                                        Call windowsfont(390, 212, 110, 0, 2, 0, "arial", Trim(caseno))   '123
                                    Case 4
                                        Call windowsfont(360, 212, 110, 0, 2, 0, "arial", Trim(caseno))   '1234
                                    Case Else
                                        Call windowsfont(330, 212, 110, 0, 2, 0, "arial", Trim(caseno))   '12345
                                End Select

                                Call sendcommand("BAR 150,315,670,4")
                                Call windowsfont(190, 317, 110, 0, 2, 0, "arial", Format(grossdef, "#,##0.0"))
                                Call windowsfont(520, 317, 110, 0, 2, 0, "arial", Format(netdef, "#,##0.0"))
                                Call sendcommand("BAR 150,430,670,4")
                                Call barcode("165", "450", "128", "50", "0", "0", "2", "2", Trim(STECBarcode)) 'x,y,code type,hight,human readable ,rotation,narrow,wide,code
                                Call windowsfont(220, 500, 30, 0, 0, 0, "arial", Trim(STECBarcode))
                                If pdremark <> "" Then
                                    Call windowsfont(480, 465, 40, 0, 2, 0, "arial", "T:" & Format(boxtare, "#,###.00") + ",N:" & Format(netreal, "#,###.00") + "," & pdremark)
                                Else
                                    Call windowsfont(480, 465, 40, 0, 2, 0, "arial", "T:" & Format(boxtare, "#,###.00") + ",N:" & Format(netreal, "#,###.00"))
                                End If
                                Call sendcommand("BAR 50,535,774,4")
                                Call sendcommand("BAR 470,315,4,220")
                                Call windowsfont(65, 540, 30, 0, 0, 0, "arial", "Effective : 28-10-2013")
                                Call windowsfont(650, 545, 30, 0, 0, 0, "arial", "FM-PCS-17")
                                Call windowsfont(65, 565, 30, 0, 0, 0, "arial", "D/M/Y")
                                Call printlabel("1", "2")
                                Call closeport()
                                '=============================================================================
                                '=============================================================================
                            End If
                        Next
                    End If
                End If
            ElseIf ChkRENew.Checked = True Then 'ให้ปรินส์ในรูปแบบที่แก้ไข
                If (SelectedRowCount > 0) Then
                    For i As Integer = 0 To SelectedRowCount - 1
                        Dim grade = GradeTextBox.Text
                        Dim caseno = CaseNoTextBox.Text
                        Dim grossreal = MetroGrid.SelectedRows(i).Cells(9).Value.ToString()
                        Dim pdremark = MetroGrid.SelectedRows(i).Cells(10).Value.ToString()
                        Dim bc = ""
                        Dim netreal = 0.0
                        Dim netdef = 0.0
                        Dim grossdef = 0.0
                        Dim boxtare = 0.0

                        If BarcodeTextBox.Text = "" Then
                            bc = MetroGrid.SelectedRows(i).Cells(2).Value.ToString()
                        Else
                            bc = BarcodeTextBox.Text
                        End If

                        If NetDefTextBox.Text = "" Then
                            netdef = MetroGrid.SelectedRows(i).Cells(4).Value.ToString()
                        Else
                            netdef = NetDefTextBox.Text
                        End If

                        If NetRealTextBox.Text = "" Then
                            netreal = NetRealTextBox.Text
                        Else
                            netreal = MetroGrid.SelectedRows(i).Cells(5).Value.ToString()
                        End If

                        If BoxTareTextBox.Text = "" Then
                            boxtare = BoxTareTextBox.Text
                        Else
                            boxtare = MetroGrid.SelectedRows(i).Cells(7).Value.ToString()
                        End If

                        If GrossDefTextBox.Text = "" Then
                            grossdef = GrossDefTextBox.Text
                        Else
                            grossdef = MetroGrid.SelectedRows(i).Cells(8).Value.ToString()
                        End If

                        ' Call openport("TSC TTP-247")
                        Call openport(printerName)
                        Call setup("103", "76", "2.0", "1", "0", "0", "0")
                        Call sendcommand("GAP  3 mm,0")
                        Call sendcommand("DIRECTION 1")
                        Call clearbuffer()

                        Call sendcommand("BAR 50,25,4,510")
                        Call sendcommand("BAR 820,25,4,510")
                        Call sendcommand("BAR 150,210,4,325")
                        Call sendcommand("BAR 50,25,770,4")

                        Select Case Len(Trim(grade))
                            Case 4
                                Call windowsfont(300, 50, 140, 0, 2, 0, "arial", Trim(grade)) '14-A
                            Case 5
                                Call windowsfont(250, 50, 140, 0, 2, 0, "arial", Trim(grade)) '14-AB
                            Case 6
                                Call windowsfont(220, 50, 140, 0, 2, 0, "arial", Trim(grade)) '14-ABC
                            Case 7
                                Call windowsfont(180, 50, 140, 0, 2, 0, "arial", Trim(grade)) '14-ABCD
                            Case 8
                                Call windowsfont(140, 50, 140, 0, 2, 0, "arial", Trim(grade)) '14-ABCDE
                            Case 9
                                Call windowsfont(100, 50, 140, 0, 2, 0, "arial", Trim(grade)) '14-ABCDEF
                            Case 10
                                Call windowsfont(70, 60, 120, 0, 2, 0, "arial", Trim(grade)) '14-ABCDEFG
                            Case 12
                                Call windowsfont(70, 60, 100, 0, 2, 0, "arial", Trim(grade)) '
                            Case Else
                                Call windowsfont(60, 60, 90, 0, 2, 0, "arial", Trim(grade)) '
                        End Select

                        Call barcode("60", "520", "128", "60", "0", "270", "2", "2", Trim(bc))
                        Call windowsfont(120, 460, 30, 90, 0, 0, "arial", Trim(bc))
                        Call sendcommand("BAR 50,210,770,4")

                        Select Case Len(Trim(caseno))
                            Case 1
                                Call windowsfont(440, 212, 110, 0, 2, 0, "arial", Trim(caseno))   '1
                            Case 2
                                Call windowsfont(420, 212, 110, 0, 2, 0, "arial", Trim(caseno))   '12
                            Case 3
                                Call windowsfont(390, 212, 110, 0, 2, 0, "arial", Trim(caseno))   '123
                            Case 4
                                Call windowsfont(360, 212, 110, 0, 2, 0, "arial", Trim(caseno))   '1234
                            Case Else
                                Call windowsfont(330, 212, 110, 0, 2, 0, "arial", Trim(caseno))   '12345
                        End Select

                        Call sendcommand("BAR 150,315,670,4")
                        Call windowsfont(190, 317, 110, 0, 2, 0, "arial", Format(grossdef, "#,##0.0"))
                        Call windowsfont(520, 317, 110, 0, 2, 0, "arial", Format(netdef, "#,##0.0"))
                        Call sendcommand("BAR 150,430,670,4")
                        Call barcode("165", "450", "128", "50", "0", "0", "2", "2", Trim(bc)) 'x,y,code type,hight,human readable ,rotation,narrow,wide,code
                        Call windowsfont(220, 500, 30, 0, 0, 0, "arial", Trim(bc))

                        If pdremark <> "" Then
                            Call windowsfont(480, 465, 40, 0, 2, 0, "arial", "T:" & Format(boxtare, "#,###.00") + ",N:" & Format(netdef, "#,###.00") + "," & pdremark)
                        Else
                            Call windowsfont(480, 465, 40, 0, 2, 0, "arial", "T:" & Format(boxtare, "#,###.00") + ",N:" & Format(netreal, "#,###.00"))
                        End If
                        Call sendcommand("BAR 50,535,774,4")
                        Call sendcommand("BAR 470,315,4,220")

                        Call windowsfont(65, 540, 30, 0, 0, 0, "arial", "Effective : 28-10-2013")
                        Call windowsfont(650, 545, 30, 0, 0, 0, "arial", "FM-PCS-17")
                        Call windowsfont(65, 565, 30, 0, 0, 0, "arial", "D/M/Y")

                        Call printlabel("1", "2")
                        Call closeport()
                    Next
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub PrintBarcodeChamp(BC As String, Caseno As String, packingdate As String)
        Try

            Dim cusRuning As String
            Dim pd = Convert.ToDateTime(packingdate)
            Dim mdc = pd.Day.ToString().PadLeft(2, "0") + "/" +
                pd.Month.ToString().PadLeft(2, "0") + "/" +
                (pd.Year + 543).ToString().PadLeft(2, "0")
            Dim custCode As String

            Dim bInfoRow As ProcessingDataSet.sp_Packing_GET_DataByBCRow
            Me.Sp_Packing_GET_DataByBCTableAdapter.Fill(Me.ProcessingDataSet1.sp_Packing_GET_DataByBC, BC)
            bInfoRow = ProcessingDataSet1.sp_Packing_GET_DataByBC.FindBybc(BC)
            If bInfoRow IsNot Nothing Then

                Dim x As ProcessingDataSet.pd_ryo_customer_label_setupRow
                Me.pd_ryo_customer_label_setupTableAdapter.Fill(Me.ProcessingDataSet1.pd_ryo_customer_label_setup, bInfoRow.frompdno)
                x = Me.ProcessingDataSet1.pd_ryo_customer_label_setup.FindBypdno(bInfoRow.frompdno)

                If x IsNot Nothing Then
                    custCode = x.customerID
                Else
                    custCode = ""
                End If
            Else
                custCode = ""
            End If

            cusRuning = custCode + (CropComboBox.Text + 543).ToString().Substring(2, 2) + Convert.ToInt16(Caseno).ToString("D6")

            Call setup("70", "35", "2.0", "6", "0", "0", "0")
            Call sendcommand("GAP 2 mm,0")
            Call sendcommand("DIRECTION 1")
            Call clearbuffer()
            Call windowsfont(160, 20, 54, 0, 0, 0, "arial", cusRuning)
            Call barcode("153", "80", "128", "60", "0", "0", "3", "2", cusRuning)
            Call windowsfont(80, 154, 60, 0, 0, 0, "arial", "MDC : " + mdc)
            Call barcode("142", "216", "128", "40", "0", "0", "2", "2", BC)
            Call printlabel("1", "1")
            Call closeport()

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub BCTextbox_KeyDown(sender As Object, e As KeyEventArgs) Handles BCTextbox.KeyDown
        'CasenoLabel.Text = ""
        'PackgradeLabel.Text = ""
        'If e.KeyCode = Keys.Enter Then
        '    Try
        '        Dim bInfoRow As ProcessingDataSet.sp_Packing_GET_DataByBCRow
        '        Me.Sp_Packing_GET_DataByBCTableAdapter.Fill(Me.ProcessingDataSet.sp_Packing_GET_DataByBC, BCTextbox.Text)
        '        bInfoRow = ProcessingDataSet.sp_Packing_GET_DataByBC.FindBybc(BCTextbox.Text)
        '        If bInfoRow Is Nothing Then
        '            MessageBox.Show("ไม่มี Barcode ที่ระบุในระบบฐานข้อมูล กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '            Return
        '        ElseIf Not bInfoRow Is Nothing Then
        '            CasenoLabel.Text = bInfoRow.caseno
        '            PackgradeLabel.Text = bInfoRow.grade
        '            XBoxTare = bInfoRow.boxtare
        '            XGrossDef = bInfoRow.grossdef
        '            XNetDef = bInfoRow.netdef
        '            XGrossReal = bInfoRow.grossreal
        '            XPdRemark = bInfoRow.pdremark
        '        End If
        '    Catch ex As Exception
        '        MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    End Try
        'End If
    End Sub

    Private Sub MetroTile1_Click(sender As Object, e As EventArgs) Handles MetroTile1.Click
        Try
            BCTextbox.Text = ""
            CasenoLabel.Text = ""
            PackgradeLabel.Text = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CropComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CropComboBox.SelectedIndexChanged
        Try
            If CropComboBox.Text <> "" And TypeComboBox.Text <> "" Then
                If CropComboBox.Text = "All" Then
                    Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter1.Fill(ProcessingDataSet1.sp_Shipping_SEL_PackedgradeByInternalGrade, 2014, TypeComboBox.Text, 0)
                Else
                    Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter1.Fill(ProcessingDataSet1.sp_Shipping_SEL_PackedgradeByInternalGrade, CropComboBox.Text, TypeComboBox.Text, 1)
                End If
                If PackedGradeComboBox.Text <> "" Then
                    Sp_Shipping_SEL_PackedgradeByGradersTableAdapter1.Fill(ProcessingDataSet1.sp_Shipping_SEL_PackedgradeByGraders, PackedGradeComboBox.Text, "")

                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TypeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TypeComboBox.SelectedIndexChanged
        Try
            If CropComboBox.Text <> "" And TypeComboBox.Text <> "" Then
                If CropComboBox.Text = "All" Then
                    Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter1.Fill(ProcessingDataSet1.sp_Shipping_SEL_PackedgradeByInternalGrade, 0, TypeComboBox.Text, 0)
                Else
                    Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter1.Fill(ProcessingDataSet1.sp_Shipping_SEL_PackedgradeByInternalGrade, CropComboBox.Text, TypeComboBox.Text, 1)
                End If
                If PackedGradeComboBox.Text <> "" Then
                    Sp_Shipping_SEL_PackedgradeByGradersTableAdapter1.Fill(ProcessingDataSet1.sp_Shipping_SEL_PackedgradeByGraders, PackedGradeComboBox.Text, "")
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub PackedGradeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles PackedGradeComboBox.SelectedIndexChanged
        Try
            If PackedGradeComboBox.Text <> "" Then
                Sp_PD_SEL_REPRINTLABELTableAdapter1.Fill(ProcessingDataSet1.sp_PD_SEL_REPRINTLABEL, PackedGradeComboBox.Text, "")
                minCase = 0
                maxCase = 0
                printCopy = 2
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ChkRENew_CheckedChanged(sender As Object, e As EventArgs) Handles ChkRENew.CheckedChanged
        Try
            If ChkRENew.Checked = True Then
                GradeTextBox.Text = ""
                CaseNoTextBox.Text = ""
                GradeTextBox.Enabled = True
                CaseNoTextBox.Enabled = True
                GradeTextBox.Visible = True
                CaseNoTextBox.Visible = True
                MetroLabel9.Visible = True
                MetroLabel8.Visible = True

                MetroLabel10.Visible = True
                NetDefTextBox.Text = ""
                NetDefTextBox.Enabled = True
                NetDefTextBox.Visible = True

                MetroLabel11.Visible = True
                BoxTareTextBox.Text = ""
                BoxTareTextBox.Enabled = True
                BoxTareTextBox.Visible = True

                MetroLabel12.Visible = True
                GrossDefTextBox.Text = ""
                GrossDefTextBox.Enabled = True
                GrossDefTextBox.Visible = True

                MetroLabel13.Visible = True
                BarcodeTextBox.Text = ""
                BarcodeTextBox.Enabled = True
                BarcodeTextBox.Visible = True

                MetroLabel14.Visible = True
                NetRealTextBox.Text = ""
                NetRealTextBox.Enabled = True
                NetRealTextBox.Visible = True


                GradeTextBox.Focus()
            ElseIf ChkRENew.Checked = False Then
                GradeTextBox.Text = ""
                CaseNoTextBox.Text = ""
                GradeTextBox.Enabled = False
                CaseNoTextBox.Enabled = False
                GradeTextBox.Visible = False
                CaseNoTextBox.Visible = False
                MetroLabel9.Visible = False
                MetroLabel8.Visible = False

                MetroLabel10.Visible = False
                NetDefTextBox.Text = ""
                NetDefTextBox.Enabled = False
                NetDefTextBox.Visible = False

                MetroLabel11.Visible = False
                BoxTareTextBox.Text = ""
                BoxTareTextBox.Enabled = False
                BoxTareTextBox.Visible = False

                MetroLabel12.Visible = False
                GrossDefTextBox.Text = ""
                GrossDefTextBox.Enabled = False
                GrossDefTextBox.Visible = False

                MetroLabel13.Visible = False
                BarcodeTextBox.Text = ""
                BarcodeTextBox.Enabled = False
                BarcodeTextBox.Visible = False

                MetroLabel14.Visible = False
                NetRealTextBox.Text = ""
                NetRealTextBox.Enabled = False
                NetRealTextBox.Visible = False
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub MetroGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs)
        Try
            If ChkRENew.Checked = True Then
                'XBC2 = MetroGrid.Item(2, MetroGrid.CurrentCell.RowIndex).Value
                GradeTextBox.Text = MetroGrid(1, MetroGrid.CurrentCell.RowIndex).Value
                CaseNoTextBox.Text = MetroGrid(3, MetroGrid.CurrentCell.RowIndex).Value
                NetDefTextBox.Text = MetroGrid(4, MetroGrid.CurrentCell.RowIndex).Value
                BoxTareTextBox.Text = MetroGrid(7, MetroGrid.CurrentCell.RowIndex).Value
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
